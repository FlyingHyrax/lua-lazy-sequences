**Experimenting with function chaining on lazy sequences**

Writing simple generators in Lua is trivial enough.  However, higher-order
utlity functions like `map` or `filter` in libraries like Underscore or Moses
can take a generator as input, but will always return a table as output.  This
means that these functions cannot be used on infinite sequences (at least, not
without first limiting the infinite sequence to a non-infinite one).  It also
means that intermediate results in each operation in a chain of operations are
stored in memory, which is inefficient for large sequences.

My goal was to write equivalents of common collection operations that both
accept generators and return generators.  As it turns out, this is not very
difficult using Lua's coroutines.  Though, my implementation so far has a lot of
code repetition that I feel could be eliminated, I'm just not sure how.

The hardest part to wrap my head around turned out to be wrapping functions
in tables to support chaining, and I borrowed heavily from Underscore's
implementation in order to get that worked out.

All in all, it's a mess and I may never have the time to come back and properly finish it.