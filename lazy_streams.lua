--[[ Chainable operations on generators ]]

-- TODO: inconsistent naming
-- TODO upload somewhere so you don't lose it

--[[ Utlities and helpers ]]

-- returns true after it has been called count times
local function call_n(count)
    local called = 0
    return function()
        if called < count then
            called = called + 1
            return false
        else
            return true
        end
    end
end

-- returns a function that is the inverse of the input function
local function invert(f)
    return function(...)
        return not f(...)
    end
end

local function table_string(t)
    local s = "[\n"
    for k,v in pairs(t) do
        local vs
        if type(v) == "table" then
            vs = table_string(v)
        else
            vs = tostring(v)
        end
        s = s .. tostring(k) .. ": " .. vs .. ", "
    end
    return s .. "\n]"
end

local function print_t(t)
    print(table_string(t))
    local mt =getmetatable(t)
    if mt then
        print("mt: " .. table_string(mt))
    else
        print("no mt")
    end
end

-- table for all of our Stream pipelining functions
local Stream = {
    chainables = {}
}
Stream.__index = Stream
Stream.__call = function(_, iterable)
    return Stream:new(iterable)
end

-- given a generator... now you can chain on the infinite Stream
function Stream:new(generator)
    return setmetatable({source = generator}, self)
end

-- directory access the stream's backing iterator
function Stream:iter()
    return self.source
end

--[[
Convenience constructors for streams
--]]

-- A stream of integers starting at low and ending at high
-- If low is nil the stream starts at 0, if high is nil
-- the stream does not terminate
function Stream.range(low, high)
    local iter = coroutine.wrap(function()
        local current = low or 0
        while high == nil or current <= high do
            coroutine.yield(current)
            current = current + 1
        end
    end)
    return Stream:new(iter)
end

-- UNTESTED
-- TODO: more flexible... allow reading formats instead of only lines
function Stream.file_lines(filename)
    local iter = coroutine.wrap(function()
        for line in io.lines(filename) do
            coroutine.yield(line)
        end
    end)
    return Stream:new(stream_iter)
end

--[[
Terminal operations: these work on a Stream but do not produce a Stream.
! They consume the stream on which they are called
! These will not terminate on infinite Streams
--]]

-- Build a table from the Stream
function Stream:collect() --> table
    local vals = {}
    for v in self.source do
        table.insert(vals, v)
    end
    return vals
end

function Stream:count(predicate) --> Int
    local count = 0
    for v in self.source do
        if predicate(v) then
            count = count + 1
        end
    end
    return count
end

function Stream:exists(predicate) --> Boolean
    return self:find(predicate) ~= nil
end

function Stream:find(predicate) --> T?
    for v in self.source do
        if predicate(v) then
            return v
        end
    end
end

function Stream:forall(predicate) --> Bool
    for v in self.source do
        if not predicate(v) then
            return false
        end
    end
    return true
end

function Stream:flush()
    for v in self.source do
        -- no-op
    end
end

-- Combine all the elments of the Stream into a value
function Stream:reduce(start, combine) --> T
    local reduction = start
    for v in self.source do
        reduction = combine(reduction, v)
    end
    return reduction
end

-- TODO
-- returns a PAIR of sequences - elements that satisfy the predicate go in
-- the first, elements which do not satisfy the predicate go in the second
-- function Stream:partition(predicate) end
-- given a stream of pairs, returns a pair of streams (!)
-- function Stream:unzip() end
-- (are these even *possible* ?)

--[[
Chainable operations: take a stream and produce a stream.
--]]

-- create a stream by applying an operation to all elements of the source
function Stream.chainables.map(seq, op)
    return coroutine.wrap(function()
        for v in seq do
            coroutine.yield(op(v))
        end
    end)
end

-- no terminal; does not trigger the sequence.
-- we either need this to be terminal (but maybe we want to cause side-effects in the middle of a chain?)
-- or another function that does the same thing terminally (but what do we call it?)
function Stream.chainables.foreach(seq, op)
    return coroutine.wrap(function()
        for v in seq do
            op(v)
            coroutine.yield(v)
        end
    end)
end

-- produce a stream of only the elements of source for which cond(element) returns true
function Stream.chainables.filter(seq, cond)
    return coroutine.wrap(function()
        for v in seq do
            if cond(v) then
                coroutine.yield(v)
            end
        end
    end)
end

function Stream.chainables.filterNot(seq, predicate)
    return Stream.chainables.filter(seq, invert(predicate))
end

-- ignore prefix elements from the stream while a predicate is satisfied
function Stream.chainables.drop_while(seq, cond)
    return coroutine.wrap(function()
        local dropping = true
        for v in seq do
            if not dropping then
                coroutine.yield(v)
            elseif not cond(v) then
                dropping = false
                coroutine.yield(v)
            end
            -- else drop
        end
    end)
end

-- return prefeix elements as long as a predicate is satisfied
function Stream.chainables.take_while(seq, cond)
    return coroutine.wrap(function()
        for v in seq do
            if cond(v) then
                coroutine.yield(v)
            else
                break
            end
        end
    end)
end



function Stream.chainables.drop(seq, n)
    return Stream.chainables.drop_while(seq, invert(call_n(n)))
end

function Stream.chainables.take(seq, n)
    return Stream.chainables.take_while(seq, invert(call_n(n)))
end

-- it would be nice to have unzip, but then I have to return TWO streams...
function Stream.chainables.zip(seq1, seq2) --> Stream of {T1, T2}
    -- seq1 is unwrapped for us in our object chaining setup,
    -- but seq2 is not!
    local seq2 = type(seq2 == "table") and seq2.source or seq2
    return coroutine.wrap(function()
        local v1, v2
        while true do
            v1 = seq1()
            v2 = seq2()
            if v1 == nil or v2 == nil then
               break
            end
            coroutine.yield({v1, v2})
        end
    end)
end

-- wrap all the ops for chaining
for func_name, func in pairs(Stream.chainables) do
    -- ?
    Stream[func_name] = function(self, ...)
        return Stream:new(func(self.source, ...))
    end
end

-- informal testing

local naturals = Stream.range()

local function isEven(int)
    return int % 2 == 0
end

local someEven = naturals:filter(isEven):take(10)
local someOdd = naturals:filterNot(isEven):take(10)

local sums = someEven:zip(someOdd)
    :map(function(p)
        return p[1] * p[2]
    end)
    :foreach(print)
    :flush()
-- ...looks pretty good!

-- when require'd
return Stream
